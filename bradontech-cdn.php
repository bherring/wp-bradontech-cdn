<?php
/*
Plugin Name: BradonTech CDN
Description: A plugin for storing and serving content from the bradontech CDN
Author: Brad Herring
Version: 0.1
*/

include 'bradontech-cdn.admin.php';

add_filter( 'pre_option_upload_url_path', 'bradontech_cdn_upload_url' );
function bradontech_cdn_upload_url() {
        return get_option('bradontech_cdn_options', array() )['cdn_url'];
    }

?>
