<?php
class BradonTechSettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'BradonTech CDN', 
            'BradonTech CDN', 
            'manage_options', 
            'bradontech-cdn-admin', 
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'bradontech_cdn_options' );
        ?>
        <div class="wrap">
            <h1>BradonTech CDN Settings</h1>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'bradontech_cdn_options_group' );
                do_settings_sections( 'bradontech-cdn-admin' );
                submit_button();
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'bradontech_cdn_options_group', // Option group
            'bradontech_cdn_options', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'cdn_setting_section_id', // ID
            'CDN Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'bradontech-cdn-admin' // Page
        );  

        add_settings_field(
            'cdn_url', // ID
            'CDN URL', // Title 
            array( $this, 'cdn_url_callback' ), // Callback
            'bradontech-cdn-admin', // Page
            'cdn_setting_section_id' // Section           
        );   
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['cdn_url'] ) )
            $new_input['cdn_url'] = sanitize_text_field( $input['cdn_url'] );

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        #print 'Enter your settings below:';
    }
    
    /** 
     * Get the settings option array and print one of its values
     */
    public function cdn_url_callback()
    {
        printf(
            '<input type="text" id="cdn_url" name="bradontech_cdn_options[cdn_url]" value="%s" />',
            isset( $this->options['cdn_url'] ) ? esc_attr( $this->options['cdn_url']) : ''
        );
    }
}

if( is_admin() )
    $bradontech_cdn_settings_page = new BradonTechSettingsPage();
